exports.users = [
    {
        username: "user1",
        email: "user1@user.com",
        password: "user1"
    },
    {
        username: "user2",
        email: "user2@user.com",
        password: "user2"
    }
]

exports.items = [
    {
        name: "item1",
        price: 2500,
        isActive: true
    },
    {
        name: "item2",
        price: 5000,
        isActive: true
    }
]